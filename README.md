# ansible-practice

First, in the *tf* directory, change the **aws_public_key_ssh_path** variables in the terraform.tfvars file (to match your SSH key name)

Then, you'll need to go into the *tf* directory and run:
````shell script
terraform init
terraform apply
````

This will create 3 EC2 instances (and output their IPs in the console).

Before running, add your EC2 IPs in the hosts files of production in *inventories/production/hosts*.
Change the <IP_MACHINE_1>, <IP_MACHINE_2>  and <IP_MACHINE_3> by the IP of your EC2s.

Then you will be able to run the ansible-playbook to install a Apache server and a DB in those servers.

To run the ansible playbook with production inventory:
````shell script
ansible-playbook --private-key=<PATH_TO_YOUR_PRIV_KEY> -i inventories/production main.yml
````

You can now access your servers in your web browser by typing the IP directly.


After that, don't forget to destroy the resources:
````shell script
terraform destroy
````
